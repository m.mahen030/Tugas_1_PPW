from django import forms
import datetime
from .models import Account, Tag
from .init_profile import init_database_account

class Edit_Profile_Form(forms.Form):
    init_database_account()
    account = Account.objects.all()[0]

    CHOICES = tuple([(tag.id, tag.name) for tag in Tag.objects.filter(account = account.id)])

    error_messages = {"required": "This is a required field",
            }

    name_attrs = {
            'type': 'text',
            'class': 'edit-profile-name',
            'placeholder' : 'Insert name here...',
        }

    email_attrs = {
            'required' : 'this is a required field',
            'placeholder': 'ex: example@example.com',
            }

    profile_url_attrs = {
            'placeholder' : 'ex: example.com/example.jpg',
            }

    name = forms.CharField(label='Name', required=True, max_length=60,
            widget=forms.TextInput(attrs=name_attrs))
    
    birth = forms.DateField(label='Birth Date', initial=datetime.date.today, required=True)
    email = forms.EmailField(label='E-mail', widget = forms.EmailInput(attrs=email_attrs))
    profile_url = forms.URLField(label='Profile Image URL', widget=forms.URLInput(attrs = profile_url_attrs))
    tags = forms.MultipleChoiceField(label='Expertise', choices = CHOICES)

