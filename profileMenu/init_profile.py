from .models import Account, Tag
from datetime import datetime

name = "Hepzibah Smith"
birth_date = datetime(1998, 10, 30, 0, 00)
gender = "Female"
expertise = ['Marketing',  'Collector', 'Public Speaking']
unusedTag = ['Designer', 'Programmer', 'Architect']
email = 'hello@smith.com'
desc = 'I am Hepzibah Smith, a purely fictional character designed to be dummy person for PPW tasks'

def init_database_account():
    if Account.objects.all().count() == 0:
        new_account = Account(name=name, birth = birth_date, gender = "Female",
		email = email, desc = desc, imageUseDefault=True)
        new_account.save()
        for tagName in expertise:
            new_account.tag_set.create(name=tagName, checked=True)

        for tagName in unusedTag:
            new_account.tag_set.create(name=tagName, checked=False)
