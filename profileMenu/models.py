from django.db import models
from datetime import datetime

class Account(models.Model):
    #def convertTimeZone():
    #    return timezone.now() + timezone.timedelta(hours=7)

    GENDER = (('M', 'Male'), ('F', 'Female'))

    name = models.CharField(max_length = 60)
    birth = models.DateTimeField(blank=True)
    gender = models.CharField(max_length = 1, choices=GENDER, default="M")
    email = models.EmailField()
    desc  = models.TextField()
    imageUseDefault = models.BooleanField(default=True)
    url = models.TextField(default="")

class Tag (models.Model):
    name = models.CharField(max_length = 60, default="Hepzibah Smith")
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    checked = models.BooleanField(default=False)
