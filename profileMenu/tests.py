from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Tag, Account

class ProfileMenuUnitTest(TestCase):
    
    def test_profileMenu_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profileMenu_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_all_attribute_is_displayed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        
        # First Account object
        account = Account.objects.all()[0]

        # Check whether the name is displayed
        self.assertIn(account.name, html_response)
        self.assertIn(account.gender, html_response)
        self.assertIn(account.birth.strftime("%d %b"), html_response)

    def test_all_tags_are_displayed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')

        account = Account.objects.all()[0]
        tags = Tag.objects.filter(account = account.id)
        
        for t in tags:
            if (t.checked):
                self.assertIn(t.name, html_response)

    def test_home_redirect_to_profileMenu_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response,'/profile/',301,200)
    
