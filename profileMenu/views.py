from django.shortcuts import render
from datetime import datetime
from .models import Account, Tag
from django.contrib.staticfiles.templatetags.staticfiles import static
from .init_profile import init_database_account, name
#from .forms import Edit_Profile_Form

response = {'author' : name}
        
def index(request):
    init_database_account()
    account = Account.objects.all()[0]
    tags = Tag.objects.filter(account = account.id)

    response['birthday'] = account.birth.strftime("%d %b")
    response['account'] = account
    response['tags'] = tags
    if (account.imageUseDefault):
        response['imgurl'] = static("profileMenu/Profile.png")
    html = 'profileMenu/prof.html'
    return render(request, html, response)


"""
def editProfile(request):
    init_database_account()
    account = Account.objects.all()[0]
    response['editProfileForm'] = Edit_Profile_Form
    html = 'profileMenu/editProf.html'
    return render(request, html, response)

"""
