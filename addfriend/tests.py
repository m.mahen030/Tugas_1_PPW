from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, add_friend
from .models import Todo

class AddFriendUnitTest(TestCase):
	def test_add_friend_url_is_exist(self):
		response = Client().get('/addfriend/')
		self.assertEqual(response.status_code, 200)

	def test_add_friend_using_index_func(self):
		found = resolve('/addfriend/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_friend(self):
		new_friend = Todo.objects.create(title='test', description='http://test.com')
		counting_all_available_friend = Todo.objects.all().count()
		self.assertEqual(counting_all_available_friend, 1)