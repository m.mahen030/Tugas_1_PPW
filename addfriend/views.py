from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Todo_Form
from .models import Todo

response = {}
def index(request):   
    response['author'] = "Kel 14" #TODO Implement yourname
    todo = Todo.objects.all()
    response['todo'] = todo
    html = 'addfriend/addfriend.html'
    response['todo_form'] = Todo_Form
    return render(request, html, response)

def add_friend(request):
    form = Todo_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['title'] = request.POST['title']
        response['description'] = request.POST['description']
        masukkan = Todo(title=response['title'], description=response['description'])
        masukkan.save()
        return HttpResponseRedirect('/addfriend/')
    else:        
        return HttpResponseRedirect('/addfriend/')