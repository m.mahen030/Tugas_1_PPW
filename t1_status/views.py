from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status
from profileMenu.models import Account
from profileMenu.init_profile import init_database_account
from django.contrib.staticfiles.templatetags.staticfiles import static

# Create your views here.
response = {}
def index(request):
    init_database_account()
    account = Account.objects.all()[0]
    response['author'] = account.name
    status = Status.objects.all().order_by('-created_date')
    response['status'] = status
    if (account.imageUseDefault):
        response['imgurl'] = static('profileMenu/Profile.png')
    html = 'status/status.html'
    response['status_form'] = Status_Form
    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        status = Status(status=response['status'])
        status.save()
    return HttpResponseRedirect('/status/')
