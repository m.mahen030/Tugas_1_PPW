from django.db import models
from datetime import datetime

class Account(models.Model):
    name = models.CharField(max_length = 60)

class Status(models.Model):
    status = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
