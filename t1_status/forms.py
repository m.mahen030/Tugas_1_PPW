from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }

    status_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 2,
        'class': 'todo-form-textarea',
        'placeholder':'Masukkan status...'
    }

    status = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=status_attrs))
