from django.apps import AppConfig


class T1StatusConfig(AppConfig):
    name = 't1_status'
